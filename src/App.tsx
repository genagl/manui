import React, {useState, useEffect} from 'react';  
import controllers from './data/controller';
import { GET_ITEMS_ACTIONS } from './data/actionsTypes';
import { AppBar, Box } from '@mui/material';
import Likes from './components/Likes';
import { Route, Routes } from 'react-router';
import ListPage from './pages/ListPage';
import ItemPage from './pages/ItemPage';
import { useNavigate } from 'react-router-dom';
import Error from './components/Error';


const App: React.FC = () => { 
  const [col,] = useState(4)
  useEffect(() => {  
    controllers( GET_ITEMS_ACTIONS, {} )
  }, []) 

  const width: number = 300 * col
  const navigate = useNavigate()
  const onClick = () => {
    navigate('/')
  }
  return (
    <Box  className="main" >
      <AppBar position="static" color="secondary" className='top'>         
          <div onClick={onClick} className='title'> 
            <BarTitle />
          </div> 
      </AppBar>
      <Box style={{ height: '100%', width:'100%', display:'flex', position: 'relative' }}> 
        <Box className="likes-container" style={{ minWidth:`calc(100% - ${ width }px)` }}>
          <Likes />
        </Box>
        <Routes>
          <Route
            path="/"
            element={ <ListPage /> }
          />
          <Route
            path='/item/:id'
            element={ <ItemPage /> }
          />
        </Routes> 
      </Box>
      <Error />
    </Box>
  );
};

export default App;

const BarTitle = (): string => { 
  switch( window.location.pathname )
  {
    case '/':
      return 'Product list Page'
    default:
      return 'Product Page'
  } 
}
