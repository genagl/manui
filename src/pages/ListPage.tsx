import React, {useState} from 'react' 
import { FixedSizeGrid as Grid  } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';
import { IRow, ItemData } from '../data/interface'
import { useGlobalState } from '../data/state'
import Item from '../components/Item'

const ListPage = ( ) : React.ReactElement | null => {
    const [items, ] = useGlobalState( 'items' ) 
    const [col,] = useState(4)
    const width: number = 300 * col
    const Row =  ( pr:IRow ): React.ReactElement | null => { 
        const itemData: ItemData = items[pr.rowIndex * col + (pr.columnIndex || 0) ] 
        return itemData 
        ?
        <div style={pr.style}>
          <Item item={ itemData } {...pr} /> 
        </div>
        :
        null  
      }
    return <AutoSizer style={{ width, height: '100%' }} >
        {() => {
            return <Grid 
            className="List" 
            height={ window.innerHeight - 100 }
            width={ width }
            columnCount={col}
            columnWidth={282}
            rowCount={ Math.ceil( items.length / col ) }
            rowHeight={ 440 } 
            >
            { Row }
            </Grid>
        }}
    </AutoSizer> 
} 

export default ListPage