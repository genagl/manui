export interface Response {
    data: []
    headers: any
    sattus: number
}

export interface ItemData {
    name:string
    id: number
    price: number
    src: string
    isLike:boolean
}

export interface IRow {
    style: any
    columnIndex: number | undefined
    rowIndex: number 
}

export interface IItemProps {
    item: ItemData | null
}