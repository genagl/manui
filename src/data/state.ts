import { createGlobalState } from 'react-hooks-global-state';
import { ItemData } from './interface';

const { setGlobalState, useGlobalState, getGlobalState } = createGlobalState({ 
    errorMessage: '',
    items: [ ],
});

export const setErrorMessage = (msg: string) => {
    setGlobalState('errorMessage', msg);
  };
export const setItems = ( items: []) => { 
    setGlobalState('items', items);
};
export const setItem = (itemData: ItemData | null) => {
    let items: any  = getGlobalState('items').map( (item:any)  => {
        return item.id === itemData?.id 
            ? 
            {...item, isLike: !(item.isLike) } 
            : 
            item 
    }) ; 
    setGlobalState('items', items);
}

export { useGlobalState };