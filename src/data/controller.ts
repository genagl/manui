import axios from "axios"
import { GET_ITEMS_ACTIONS } from "./actionsTypes"
import { setErrorMessage, setItems } from "./state"
import { Response } from "./interface"

const SERVER_URI = `${process.env.REACT_APP_SERVER_URL }/image`
const controllers = async( action:string, data: any ) => {
    switch(action)
    {
        case GET_ITEMS_ACTIONS:
            try {
                const response: Response = await axios.get( SERVER_URI )
                const body: [] = response.data  
                setItems( body )
            }
            catch(e : any) {
                console.error(e.message) 
                if (typeof e === "string") {
                    setErrorMessage( e ) 
                } else if ( e instanceof Error ) {
                    setErrorMessage( ( e as Error ).message )
                }    
            }            
            return
        default:
            return action
    }
} 
export default controllers