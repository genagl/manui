import React from 'react' 
import { useGlobalState } from '../data/state'
import { ItemData } from '../data/interface'
import { ItemCard } from './ItemCard'

const Likes = (): React.ReactElement => {
  const [items, ] = useGlobalState('items')
  const height: number = window.innerHeight - 230
  const itemCards: React.ReactElement[] = items
    .filter((item:ItemData) => item.isLike )
    .map((item:ItemData) => <ItemCard key={item.id} item={item} />)
  return (
    <div className='likes'>
      <svg width="409" height={height+2} viewBox={`0 0 409 ${height+2}`} fill="none" xmlns="http://www.w3.org/2000/svg"  className='contour'>
          <rect x="1" y="1" width="407" height={height} rx="30" fill="none" stroke="#414141" strokeLinecap="round" strokeWidth={2} strokeLinejoin="round" strokeDasharray="12 12"/>
      </svg>
      <div className='title'>
        Favorites
      </div>
      <div className="auto-overflow-y contents" >
        {itemCards}
      </div>
    </div>
  )
}

Likes.propTypes = {}

export default Likes