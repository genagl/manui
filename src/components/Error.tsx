import React, {useEffect} from 'react' 
import { setErrorMessage, useGlobalState } from '../data/state'

const Error = (): React.ReactElement | null => {
    let time:any = null
    const [errorMessage, ] = useGlobalState('errorMessage')
    useEffect(() => {
        // eslint-disable-next-line react-hooks/exhaustive-deps
        time = setTimeout(() => {
            setErrorMessage('')
        }, 2000);
        return () => {
           clearTimeout( time ) 
        }
    }, [errorMessage])

    
    return (
        errorMessage
            ?
            <div className="error-message" >
            { errorMessage }
            </div>
            :
            null
      );
} 

export default Error