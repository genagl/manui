import React from 'react' 
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia'; 
import Typography from '@mui/material/Typography';
import { useNavigate  } from 'react-router-dom'; 
import { IItemProps } from '../data/interface';

const serv = process.env.REACT_APP_SERVER_URL

export const ItemCard = ( props:IItemProps ) => {    
    const navigate = useNavigate()
    const onClick = () => {
        navigate(`item/${ props.item?.id }`)
    }
    return (
        <Card sx={{ display: 'flex' }} className="item-like-card" onClick={onClick}>
            <CardMedia
                component="img"
                sx={{ width: 108, height:108 }}
                image={ `${serv}${props.item?.src}` }
                alt="thumb"
            />
            <Box sx={{ display: 'flex', flexDirection: 'column', width: '100%', padding: '0 20px' }}>
                <CardContent sx={{ flex: '1 0 auto' }} className='item-like-card-content'>
                <Typography component='div' className='title'>
                    {props.item?.name}
                </Typography>
                <div className='item-like-card-content-price'>
                    <Typography variant='subtitle1' color='text.secondary' component='div' className='price'>
                        $ {props.item?.price}
                    </Typography>            
                    <svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect x="3" y="3" width="30" height="30" rx="7" fill="#414141"/>
                        <path d="M6.75 2.25C5.55653 2.25 4.41193 2.72411 3.56802 3.56802C2.72411 4.41193 2.25 5.55653 2.25 6.75V29.25C2.25 30.4435 2.72411 31.5881 3.56802 32.432C4.41193 33.2759 5.55653 33.75 6.75 33.75H29.25C30.4435 33.75 31.5881 33.2759 32.432 32.432C33.2759 31.5881 33.75 30.4435 33.75 29.25V6.75C33.75 5.55653 33.2759 4.41193 32.432 3.56802C31.5881 2.72411 30.4435 2.25 29.25 2.25H6.75ZM18 12.9825C18 12.9825 19.5863 10.5413 21.645 10.1813C26.7525 9.2925 28.8562 13.7587 28.0012 17.0775C26.4825 23.0063 18 28.1812 18 28.1812C18 28.1812 9.5175 23.0063 7.99875 17.0887C7.155 13.77 9.25875 9.2925 14.355 10.1925C16.4138 10.5525 18 12.9825 18 12.9825Z" fill="#FFCC26"/>
                    </svg>
                </div>
                
                </CardContent> 
            </Box>
        </Card>
    )
}
